// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SpaceGameHUDBase.generated.h"

/**
 * 
 */
UCLASS()
class PG25YEWON_UNREAL_A2_API ASpaceGameHUDBase : public AHUD
{
	GENERATED_BODY()
	
};
