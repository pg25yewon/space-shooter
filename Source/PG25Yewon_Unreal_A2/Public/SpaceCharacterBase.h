
// Copyright Yewon Jung 2024 

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ScoreComponent.h"
#include "SpaceCharacterBase.generated.h"

class UHealthComponent;
class ABullet;
class USceneComponent;
class UInputAction;

UCLASS()
class PG25YEWON_UNREAL_A2_API ASpaceCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASpaceCharacterBase();
	
	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UHealthComponent> HealthComponent;

	UFUNCTION(Server, Reliable)
	void Shoot(const FInputActionValue& Value);

	UPROPERTY(EditAnywhere, BlueprintReadOnly , Category = "Shooting")
	TSubclassOf<ABullet> BulletClassToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly ,  Category = "Shooting")
	TObjectPtr<USceneComponent> ShootPosition;

	UPROPERTY(EditAnywhere,  BlueprintReadOnly , Category = "Shooting")
	TObjectPtr<UInputAction> ShootAction;

	virtual void Destroyed() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor);
	
public:	 
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};