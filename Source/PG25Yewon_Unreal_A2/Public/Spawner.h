// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"


class AEnemyCharacterBase;
//class SpaceCharacterBase;

UCLASS()
class PG25YEWON_UNREAL_A2_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SpawnDelay;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AEnemyCharacterBase> ClassToSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
	void SpawnEnemy();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	
	FTimerHandle SpawnHandle;

};
