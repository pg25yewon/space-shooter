// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SpaceGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PG25YEWON_UNREAL_A2_API USpaceGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
