// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/DamageType.h"
#include "Bullet.generated.h"

class UDamageType;
UCLASS()
class PG25YEWON_UNREAL_A2_API ABullet : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	
	// Movement speed of the bullet
	UPROPERTY(EditDefaultsOnly, Category = "Bullet")
	float BulletSpeed = 1000.0f;

	// Time after which the bullet destroys itself
	UPROPERTY(EditDefaultsOnly, Category = "Bullet")
	float LifeSpan = 0.3f;
	
	void DestroyBullet();

	UPROPERTY(VisibleAnywhere)
	USphereComponent* CollisionComponent;

	UFUNCTION()
	void OnBulletOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Timer handle for bullet lifespan
	FTimerHandle LifeSpanTimerHandle;
};

