//Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "Engine/InputActionDelegateBinding.h"
#include "GameFramework/PlayerController.h"
#include "SpaceControllerBase.generated.h"

/**
 * 
 */

class UInputMappingContext;
class UInputAction;

UCLASS()
class PG25YEWON_UNREAL_A2_API ASpaceControllerBase : public APlayerController
{
	GENERATED_BODY()

public:
	ASpaceControllerBase(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	TObjectPtr <UInputMappingContext> MovementInputContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	TObjectPtr <UInputAction> MovementInputAction;

protected:

	virtual void SetupInputComponent() override;

	UFUNCTION()
	void HorizontalMove(const FInputActionValue& Value);
	
};
