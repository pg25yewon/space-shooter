// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverScreen.generated.h"

/**
 * 
 */
UCLASS()
class PG25YEWON_UNREAL_A2_API UGameOverScreen : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	void Restart();
};
