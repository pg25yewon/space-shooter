// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "SpaceGameModeBase.generated.h"

/**
 * 
 */

UCLASS()
class PG25YEWON_UNREAL_A2_API ASpaceGameModeBase : public AGameMode
{
	GENERATED_BODY()

public:
	ASpaceGameModeBase(const FObjectInitializer& ObjectInitializer);
	
};
