// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SpacePlayerState.generated.h"

/**
 * 
 */

class UScoreComponent;
UCLASS()
class PG25YEWON_UNREAL_A2_API ASpacePlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	ASpacePlayerState(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UScoreComponent> ScoreComponent;
	
};
