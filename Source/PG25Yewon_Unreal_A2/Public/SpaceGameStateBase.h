// Copyright Yewon Jung 2024 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "SpaceGameStateBase.generated.h"

/**
 * 
 */

class UUserWidget;
UCLASS()
class PG25YEWON_UNREAL_A2_API ASpaceGameStateBase : public AGameState
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UUserWidget> EndGameScreenClass;

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void EndGame();

	UFUNCTION(NetMulticast, Reliable)
	void EndGameClient();
	
	
};
