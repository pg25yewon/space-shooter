// Copyright Yewon Jung 2024 


#include "SpaceControllerBase.h"

#include "SpaceCameraManager.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "InputAction.h"
#include "GameFramework/Character.h"

ASpaceControllerBase::ASpaceControllerBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerCameraManagerClass = ASpaceCameraManager::StaticClass();
}

void ASpaceControllerBase::SetupInputComponent()
{
	Super::SetupInputComponent();

	
	UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());

	EnhancedInputSubsystem->ClearAllMappings();
	EnhancedInputSubsystem->AddMappingContext(MovementInputContext, 0);

	UEnhancedInputComponent* InputComp = Cast<UEnhancedInputComponent>(InputComponent);
	if(IsValid(InputComp))
	{
		if(IsValid(MovementInputAction))
		{
			InputComp->BindAction(MovementInputAction, ETriggerEvent::Triggered, this, &ThisClass::HorizontalMove);
		}
	}
}

void ASpaceControllerBase::HorizontalMove(const FInputActionValue& Value)
{
	float horizontalAxisValue = Value.Get<float>();
	ACharacter* possesedCharacter = GetCharacter();
	if(IsValid(possesedCharacter))
	{
		possesedCharacter->AddMovementInput(possesedCharacter->GetActorForwardVector(),horizontalAxisValue);
	}
}
