// Copyright Yewon Jung 2024 

#include "SpaceCharacterBase.h"

#include "Bullet.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Kismet/GameplayStatics.h"
#include "HealthComponent.h"
#include "SpaceGameStateBase.h"

// Sets default values
ASpaceCharacterBase::ASpaceCharacterBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	ShootPosition = CreateDefaultSubobject<USceneComponent>(TEXT("Shoot Position"));

	ShootPosition->SetupAttachment(RootComponent);
	
	HealthComponent->SetIsReplicated(true);
}

void ASpaceCharacterBase::Shoot_Implementation(const FInputActionValue& Value)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnParameters.Owner = this;
	GetWorld()->SpawnActor<ABullet>(BulletClassToSpawn, ShootPosition->GetComponentLocation(), FRotator::ZeroRotator, SpawnParameters);
}

void ASpaceCharacterBase::Destroyed()
{
	Super::Destroyed();

	TObjectPtr<ASpaceGameStateBase> GameState =  GetWorld()->GetGameState<ASpaceGameStateBase>();
	if(HasAuthority() && IsValid(GameState))
	{
		GameState->EndGame();
	}
}

// Called when the game starts or when spawned
void ASpaceCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	if (IsValid(HealthComponent))
	{
		OnTakeAnyDamage.AddUniqueDynamic(HealthComponent.Get(), &UHealthComponent::TakeDamage);
		OnActorBeginOverlap.AddUniqueDynamic(this,&ThisClass::OnOverlapBegin);
	}
}

void ASpaceCharacterBase::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor && OtherActor != this)
	{
		UGameplayStatics::ApplyDamage(OtherActor,1.0f,nullptr,this,nullptr);
	}
}

// Called every frame
void ASpaceCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASpaceCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	UEnhancedInputComponent* InputComp = Cast<UEnhancedInputComponent>(InputComponent);
	if(IsValid(InputComp))
	{
		if(IsValid(ShootAction))
		{
			InputComp->BindAction(ShootAction, ETriggerEvent::Triggered, this, &ThisClass::Shoot);
		}
	}

}