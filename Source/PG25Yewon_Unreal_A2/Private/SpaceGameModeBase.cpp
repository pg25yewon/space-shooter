// Copyright Yewon Jung 2024 


#include "SpaceGameModeBase.h"
#include "SpaceControllerBase.h"
#include "SpaceGameStateBase.h"
#include "SpacePlayerState.h"
#include "SpaceCharacterBase.h"
#include "SpaceGameHUDBase.h"

ASpaceGameModeBase::ASpaceGameModeBase(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	GameStateClass = ASpaceGameStateBase::StaticClass();
	PlayerStateClass = ASpacePlayerState::StaticClass();
	PlayerControllerClass = ASpaceControllerBase::StaticClass();
	DefaultPawnClass = ASpaceCharacterBase::StaticClass();
	HUDClass = ASpaceGameHUDBase::StaticClass();
}
