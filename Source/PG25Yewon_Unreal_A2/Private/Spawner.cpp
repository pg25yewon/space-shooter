// Copyright Yewon Jung 2024 


#include "Spawner.h"
#include "EnemyCharacterBase.h"
#include "SpaceCharacterBase.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	if(HasAuthority())
	{
		GetWorldTimerManager().SetTimer(SpawnHandle, this, &ThisClass::SpawnEnemy, SpawnDelay, true);
	}
}

void ASpawner::SpawnEnemy_Implementation()
{
	FActorSpawnParameters MySpawnParams;
	MySpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	GetWorld()->SpawnActor<AEnemyCharacterBase>(ClassToSpawn, GetActorLocation() + FVector(0.0f,100.0f,0.0f),FRotator::ZeroRotator, MySpawnParams);
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

