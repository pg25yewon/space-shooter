// Copyright Yewon Jung 2024 

#include "ScoreComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties

UScoreComponent::UScoreComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}

void UScoreComponent::AddScore_Implementation(const int32 scoreToAdd)
{
	score+=scoreToAdd;
}


// Called when the game starts
void UScoreComponent::BeginPlay()
{
	Super::BeginPlay();
	score = 0;
	// ...
	
}


// Called every frame
void UScoreComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UScoreComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UScoreComponent, score);
}


