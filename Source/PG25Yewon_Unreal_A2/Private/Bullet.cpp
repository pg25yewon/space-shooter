// Copyright Yewon Jung 2024 

#include "Bullet.h"
#include "SpaceCharacterBase.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABullet::ABullet()
{
	// Set this actor to call Tick() every frame
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	CollisionComponent->SetSphereRadius(10.0f); 
	RootComponent = CollisionComponent;
	
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnBulletOverlap);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(LifeSpanTimerHandle, this, &ABullet::DestroyBullet, LifeSpan, false);

	FVector BulletDirection = GetActorForwardVector() * BulletSpeed;
	
	SetActorLocation(GetActorLocation() + BulletDirection * GetWorld()->GetDeltaSeconds());
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Function to handle bullet destruction
void ABullet::DestroyBullet()
{
	Destroy();
}


void ABullet::OnBulletOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(!HasAuthority())
	{
		return;
	}
	if (OtherActor && OtherActor != this)
	{
		const float damageDone = UGameplayStatics::ApplyDamage(OtherActor,10.0f,nullptr,this,nullptr);
		TObjectPtr<ASpaceCharacterBase> OwningSpaceCharacter = Cast<ASpaceCharacterBase>(GetOwner());
		if(IsValid(OwningSpaceCharacter))
		{
			const TObjectPtr<APlayerState> PlayerState =  OwningSpaceCharacter->GetPlayerState();
			if(IsValid(PlayerState))
			{
				TObjectPtr<UScoreComponent> ScoreComponent = PlayerState->FindComponentByClass<UScoreComponent>();
				if(IsValid(ScoreComponent))
				{
					ScoreComponent->AddScore(10);
				}
			}
		}
		Destroy();
	}
}
