// Copyright Yewon Jung 2024 


#include "SpaceGameStateBase.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"

void ASpaceGameStateBase::EndGameClient_Implementation()
{
	TObjectPtr<APlayerController> PlayerController = GetWorld()->GetFirstPlayerController();
	if(!IsValid(EndGameScreenClass))
	{
		return;
	}
	TObjectPtr<UUserWidget> EndGameScreen = CreateWidget<UUserWidget>(PlayerController,EndGameScreenClass);
	EndGameScreen->AddToViewport();
}

void ASpaceGameStateBase::EndGame_Implementation()
{
	EndGameClient();
}
