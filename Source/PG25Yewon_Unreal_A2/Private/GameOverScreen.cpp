// Copyright Yewon Jung 2024 


#include "GameOverScreen.h"

void UGameOverScreen::Restart()
{
	GetWorld()->GetGameInstance()->ReturnToMainMenu();
}
