// Copyright Yewon Jung 2024 


#include "SpaceCameraManager.h"
#include "GameFramework/GameState.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"

void ASpaceCameraManager::UpdateViewTargetInternal(FTViewTarget& OutVT, float DeltaTime)
{
	FVector camLoc = GetCameraLocation();
	FRotator camRot = GetCameraRotation();

	if(!IsValid (CameraData))
	{
		return;
	}

	// Tarray = unreal version of array
	TArray<TObjectPtr<AActor>> pawns;

	const TObjectPtr<UWorld> world = GetWorld();

	if(!IsValid(world))
	{
		return;
	}

	const TObjectPtr<AGameStateBase> gameState = world ->GetGameState();
	if(!IsValid(gameState))
	{
		return;
	}

	// PlayerArray is a TArray of tobjectptr self player state
	// in this camera manager i'm fetching every player that is connected to networking

	// getting all the characters and adding the position to the list
	for(const TObjectPtr<APlayerState> playerStateIter : gameState->PlayerArray)
	{
		TObjectPtr<APawn> pawnIter = playerStateIter->GetPawn();
		if(!IsValid(pawnIter))
		{
			continue;
		}
		pawns.AddUnique(pawnIter);
	}
	
	FVector avgLoc = UGameplayStatics::GetActorArrayAverageLocation(pawns);
	avgLoc += CameraData->CameraOffset;

	FVector lerpLocation = FMath::VInterpTo(GetCameraLocation(), avgLoc, DeltaTime, CameraData->CameraDelay);

	camRot = CameraData->CameraRotationOffset;

	OutVT.POV.Location = avgLoc;
	OutVT.POV.Rotation = camRot;
	OutVT.POV.FOV = 90.0f;
	
}