// Copyright Yewon Jung 2024 


#include "SpacePlayerState.h"
#include "ScoreComponent.h"

ASpacePlayerState::ASpacePlayerState(const FObjectInitializer& ObjectInitializer)
{
	ScoreComponent = CreateDefaultSubobject<UScoreComponent>(TEXT("Score Component"));
}